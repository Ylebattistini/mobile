# README #

CityMap è un'applicazione per sistemi Android che permette all'utente dopo essersi registrato, anche tramite Facebook, di usufruire di un servizio di mapping che mostra i luoghi di culto, di interesse, di svago ecc 
nei dintorni.
CityMap permette inoltre di filtrare la ricerca di questi luoghi andando a nascondere eventuali icone di non interesse per l'utente.
Il profilo dell'utente è composto da poche semplici informazioni che nel corso dell'utilizzo possono anche essere modificate.

Nel momento in cui l'utente trova di suo interesse un luogo( segnalato da un bollino di colore diverso in base alla categoria di appartenenza) può cliccarci sopra andando ad aprire una pagina web contente tutte le informazioni che lo riguardano.
Ad esempio per i luoghi di interesse generale ci siamo rifatte a Wikipedia mentre per i ristoranti, bar, caffetterie ecc a TripAdvisor.

Ovviamente questa app può essere ampliata.
Alcuni ampliamenti che abbiamo in mente sono:
-la possibilità di avere una chat all'interno dell'app per poter condividere la posizione con coloro che utilizzano il servizio
-cliccando sul simbolo del luogo ottenere il percorso per arrivarci
-visualizzare le posizioni di altri utenti
-ottenere un notifica quando si è vicini ad un luogo.
