package com.example.citymap.main.local_db;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

public interface DBManager<T, IDType extends Serializable> {

    /**
     * se l'oggetto da salvare è gia presente nel db lo aggiorna, altrimenti lo inserisce
     * @param tuple tupla da salvare
     * @return esito dell'operazione
     */
    boolean save(T tuple);

    /**
     * per ogni oggetto presente nella lista passata come parametro, se questo esiste viene aggiornato, altrimenti inserito
     * @param tuple lista di tuple da salvare
     * @return ritorna falso se fallisce almeno un salvataggio
     */
    boolean save(List<T> tuple);

    /**
     * inserisce un'elemento
     * @param tuple tupla da inserire
     * @return l'esito dell'operazione
     */
    boolean insert(T tuple);

    /**
     * aggiorna un'elemento
     * @param tuple tupla da aggiornare
     * @return l'esito dell'operazione
     */
    boolean update(T tuple);

    /**
     *
     * @param tuple tupla da aggiornare
     * @param whereCondition stringa di condizione ex: nomeColonna1=? and nomecolonna2=?
     * @param whereArgument array di stringhe che corrispondono ai valori da sostiuire agli eventuali placeholder presenti nella stringa di condizoine
     * @return l'esito dell'operazione
     */
    boolean update(String table,T tuple, String whereCondition, String[] whereArgument);

    /**
     * ricerca un elemento caratterizzato da un id
     * @param id l'id dell'elemento da ricercare
     * @return l'elemento carraterizzato dall'id passato in ingresso; ritorna null se l'elemento non è presente
     */
    T findOne(IDType id);

    /**
     * cancella tutte le righe che soddisfano la condizione di selezione (operazione dell'algebra relazionale)
     * @param whereCondition stringa di condizione ex: nomeColonna1=? and nomecolonna2=?
     * @param whereArgument
     * @return
     */
    boolean delete(String whereCondition, String[] whereArgument);

    /**
     * cancella la tupla caratterizzata da un determinato id
     * @param id
     * @return
     */
    boolean delete(IDType id);

    /**
     * cancella la tupla corrispondente ad un elemento passatyo come parametro.
     * @param tuple elemento da cancellare
     * @return esito della cancellazione
     */
    boolean delete(T tuple);

    /**
     *
     * @param whereCondition
     * @return la lista con gli elementi che soddisfano la clausola where indicata
     */
    List<T> findAll(Map<String, String> whereCondition);

    /**
     * restituisce tutte le tuple di una determinata tabella
     * @return la lista con tutti gli elementi desiderati o una lista vuota se nessun elemento è presente
     */
    List<T> findAll();

    /**
     * Restiuisce gli elementi che soddisfano le clausole di selezione, ordinate secondo un certo criterio
     * @param whereCondition
     * @param values
     * @param orderBy
     * @return la lista
     */
    List<T> findAll(String whereCondition, String[] values, String orderBy);
}
