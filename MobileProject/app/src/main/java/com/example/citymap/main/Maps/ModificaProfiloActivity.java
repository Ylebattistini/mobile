package com.example.citymap.main.Maps;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.citymap.R;
import com.example.citymap.main.local_db.User;
import com.example.citymap.main.local_db.UserDBManager;

public class ModificaProfiloActivity extends AppCompatActivity {

    private TextView nome;
    private TextView cognome;
    private TextView citta;
    private TextView username;
    private TextView password;
    private TextView confirmedPassword;

    private String nomeString;
    private String cognomeString;
    private String cittaString;
    private String usernameString;
    private String passwordString;
    private String confirmedPasswordString;

    private Button back;
    private Button save;

    private UserDBManager dbmanager;
    private boolean updateData;
    private User utente;
    private String userUSERNAME;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modifica_profilo);
        dbmanager= new UserDBManager(ModificaProfiloActivity.this);
        initComponentView();

        this.back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tornaIndietro();
            }
        });

        this.save.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.GINGERBREAD)
            @Override
            public void onClick(View v) {

                if(nome.getText().toString().isEmpty()|| cognome.getText().toString().isEmpty() || citta.getText().toString().isEmpty() || username.getText().toString().isEmpty() || password.getText().toString().isEmpty() || confirmedPassword.getText().toString().isEmpty()){
                    Toast.makeText(ModificaProfiloActivity.this, "Campi obbligatori non compilati", Toast.LENGTH_LONG).show();
                } else{
                    nomeString = nome.getText().toString();
                    cognomeString = cognome.getText().toString();
                    cittaString = citta.getText().toString();
                    usernameString = username.getText().toString();
                    passwordString = password.getText().toString();
                    confirmedPasswordString = confirmedPassword.getText().toString();
                    if(confirmedPasswordString.equals(passwordString)){
                        utente= new User(nomeString, cognomeString, cittaString, usernameString, passwordString);
                        modificaProfilo(utente, userUSERNAME);
                    }else{
                        Toast.makeText(ModificaProfiloActivity.this, "Le password non coincidono", Toast.LENGTH_LONG).show();
                        confirmedPassword.clearComposingText();
                        password.clearComposingText();
                    }
                }
                finish();
            }
        });

    }

    private void tornaIndietro() {
        Intent intent = new Intent(ModificaProfiloActivity.this, HamburgerActivity.class);
        startActivity(intent);
    }

    private void modificaProfilo(User users, String  name) {
        this.dbmanager= new UserDBManager(this);
        this.updateData= this.dbmanager.update(users);
        this.dbmanager.setUsername(name);
        Intent intent = new Intent(ModificaProfiloActivity.this, HamburgerActivity.class);
        startActivity(intent);
    }

    private void initComponentView() {
        this.nome=findViewById(R.id.Name);
        this.cognome=findViewById(R.id.Surname);
        this.citta=findViewById(R.id.City);
        this.username=findViewById(R.id.UserName);
        this.password=findViewById(R.id.Password);
        this.confirmedPassword=findViewById(R.id.ConfirmPassword);

        this.back = findViewById(R.id.Back);
        this.save = findViewById(R.id.Save);

        this.utente=this.dbmanager.getUserInformation();
        this.nomeString=this.utente.getName();
        this.cognomeString=this.utente.getSurname();
        this.cittaString=this.utente.getCity();
        this.usernameString=this.utente.getUsername();
        this.passwordString=this.utente.getPassword();
        this.confirmedPasswordString=this.utente.getPassword();

        this.nome.setText(this.nomeString);
        this.cognome.setText(this.cognomeString);
        this.citta.setText(this.cittaString);
        this.username.setText(this.usernameString);
        this.password.setText(this.passwordString);
        this.confirmedPassword.setText(this.confirmedPasswordString);

    }

}
