package com.example.citymap.main;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.citymap.R;
import com.example.citymap.main.Maps.HamburgerActivity;
import com.example.citymap.main.Maps.PermissionRequest;
import com.example.citymap.main.local_db.UserDBManager;

public class LoginActivity extends AppCompatActivity {

    private TextView username;
    private TextView password;
    private Button enter;
    private String usernameUser;
    private String passwordUser;

    private UserDBManager dbmanagers;
    private Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initComponentView();

        this.enter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signIn();
            }
        });

    }

    private void signIn() {
        if(this.password.getText().toString().isEmpty() && this.username.getText().toString().isEmpty()){
            Toast.makeText(this, "Campi obbligatori non inseriti", Toast.LENGTH_LONG).show();
        }else{
            this.usernameUser = this.username.getText().toString();
            this.passwordUser = this.password.getText().toString();
            this.dbmanagers= new UserDBManager(this);
           if(this.dbmanagers.checkUsername(this.usernameUser)){
               if(this.dbmanagers.checkPassword(this.passwordUser)){
                   Toast.makeText(this, "Accesso corretto", Toast.LENGTH_LONG).show();
                   this.dbmanagers.setSessione(this.usernameUser);
                   if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED ) {
                       intent = new Intent(LoginActivity.this, HamburgerActivity.class);
                       startActivity(intent);
                   }else{
                       intent = new Intent(LoginActivity.this, PermissionRequest.class);
                       startActivity(intent);
                   }
               }else{
                   Toast.makeText(this, "Password errata", Toast.LENGTH_LONG).show();
                   this.password.setText("");
               }
           }else{
               Toast.makeText(this, "Username o password errati", Toast.LENGTH_LONG).show();
               this.username.setText("");
               this.password.setText("");
           }
        }
    }

    private void initComponentView() {
        this.password=findViewById(R.id.passwordUtente);
        this.username=findViewById(R.id.usernameutente);
        this.enter= findViewById(R.id.entra);
    }
}
