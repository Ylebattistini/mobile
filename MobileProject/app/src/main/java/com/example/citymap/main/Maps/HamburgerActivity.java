package com.example.citymap.main.Maps;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.text.Editable;
import android.text.Layout;
import android.text.TextWatcher;
import android.util.Log;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.Toast;

import com.example.citymap.R;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


import com.example.citymap.main.MainActivity;
import com.example.citymap.main.local_db.UserDBManager;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.AutocompletePrediction;
import com.google.android.libraries.places.api.model.AutocompleteSessionToken;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.model.TypeFilter;
import com.google.android.libraries.places.api.net.FetchPlaceRequest;
import com.google.android.libraries.places.api.net.FetchPlaceResponse;
import com.google.android.libraries.places.api.net.FindAutocompletePredictionsRequest;
import com.google.android.libraries.places.api.net.FindAutocompletePredictionsResponse;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.android.libraries.places.widget.AutocompleteSupportFragment;
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener;
import com.mancj.materialsearchbar.MaterialSearchBar;
import com.mancj.materialsearchbar.adapter.SuggestionsAdapter;


public class HamburgerActivity<HeaderView> extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,  OnMapReadyCallback{


    private AlertDialog alertDialog1;
    private String[] values = {"Shopping","Restaurant","Museum", "Bar", "Library", "Train Station"};
    boolean[] checkedItems = {};
    private String selected;
    private  String filter;
    private TextView userUsername;
    private UserDBManager dbManager;
    private static String userNamed;

    private String mapFiltering=null;
    private  SupportMapFragment mapFragment;
    private List<Place.Field> placeFields;
    private AutocompleteSupportFragment autocompleteSupportFragment;
    private List<Address> lista;
    private MarkerOptions marker;
    private PlacesClient placeClient;

    private Location location;
    private GoogleMap mMap;
    private String city;
    private LocationManager locationManager;
    private AutocompleteSessionToken token;

    //TODO:prove
    private MaterialSearchBar materialSearchBar;
    private final float DEFAULT_ZOOM = 15;
    private List<AutocompletePrediction> predictionList;
    int PROXIMITY_RADIUS = 10000;
    //TODO: fine prove

    private LatLng place;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_hamburger);
        checkedItems = new boolean[values.length];

        initComponentView();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

       /* autocompleteSupportFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(@NonNull Place place) {
                final LatLng latlang= place.getLatLng();
                Log.d("yle", "onPlaceSelected"+latlang.latitude +"\n"+latlang.longitude);
                //addMarkerMy(latlang.latitude, latlang.longitude);
            }

            @Override
            public void onError(@NonNull Status status) {
                Log.d("yle", "notSelecteed");
            }
        });*/
    //TODO: prove
        materialSearchBar.setOnSearchActionListener(new MaterialSearchBar.OnSearchActionListener() {
            @Override
            public void onSearchStateChanged(boolean enabled) {

            }

            @Override
            public void onSearchConfirmed(CharSequence text) {
                startSearch(text.toString(), true, null, true);
            }

            @Override
            public void onButtonClicked(int buttonCode) {
                if (buttonCode == MaterialSearchBar.BUTTON_NAVIGATION) {
                    //opening or closing a navigation drawer
                } else if (buttonCode == MaterialSearchBar.BUTTON_BACK) {
                    materialSearchBar.disableSearch();
                }
            }
        });

        materialSearchBar.addTextChangeListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                FindAutocompletePredictionsRequest predictionsRequest = FindAutocompletePredictionsRequest.builder()
                        .setTypeFilter(TypeFilter.ADDRESS)
                        .setSessionToken(token)
                        .setQuery(s.toString())
                        .build();
                placeClient.findAutocompletePredictions(predictionsRequest).addOnCompleteListener(new OnCompleteListener<FindAutocompletePredictionsResponse>() {
                    @Override
                    public void onComplete(@NonNull Task<FindAutocompletePredictionsResponse> task) {
                        if (task.isSuccessful()) {
                            FindAutocompletePredictionsResponse predictionsResponse = task.getResult();
                            if (predictionsResponse != null) {
                                predictionList = predictionsResponse.getAutocompletePredictions();
                                List<String> suggestionsList = new ArrayList<>();
                                for (int i = 0; i < predictionList.size(); i++) {
                                    AutocompletePrediction prediction = predictionList.get(i);
                                    suggestionsList.add(prediction.getFullText(null).toString());
                                }
                                materialSearchBar.updateLastSuggestions(suggestionsList);
                                if (!materialSearchBar.isSuggestionsVisible()) {
                                    materialSearchBar.showSuggestionsList();
                                }
                            }
                        } else {
                            Log.i("mytag", "prediction fetching task unsuccessful");
                        }
                    }
                });
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        materialSearchBar.setSuggstionsClickListener(new SuggestionsAdapter.OnItemViewClickListener() {
            @Override
            public void OnItemClickListener(int position, View v) {
                if (position >= predictionList.size()) {
                    return;
                }
                AutocompletePrediction selectedPrediction = predictionList.get(position);
                String suggestion = materialSearchBar.getLastSuggestions().get(position).toString();
                materialSearchBar.setText(suggestion);

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        materialSearchBar.clearSuggestions();
                    }
                }, 1000);
                InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                if (imm != null)
                    imm.hideSoftInputFromWindow(materialSearchBar.getWindowToken(), InputMethodManager.HIDE_IMPLICIT_ONLY);
                final String placeId = selectedPrediction.getPlaceId();
                List<Place.Field> placeFields = Arrays.asList(Place.Field.LAT_LNG);

                FetchPlaceRequest fetchPlaceRequest = FetchPlaceRequest.builder(placeId, placeFields).build();
                placeClient.fetchPlace(fetchPlaceRequest).addOnSuccessListener(new OnSuccessListener<FetchPlaceResponse>() {
                    @Override
                    public void onSuccess(FetchPlaceResponse fetchPlaceResponse) {
                        Place place = fetchPlaceResponse.getPlace();
                        Log.i("mytag", "Place found: " + place.getName());
                        LatLng latLngOfPlace = place.getLatLng();
                        if (latLngOfPlace != null) {
                            addMarkerMy(latLngOfPlace.latitude, latLngOfPlace.longitude);
                           // mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLngOfPlace, DEFAULT_ZOOM));
                        }
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        if (e instanceof ApiException) {
                            ApiException apiException = (ApiException) e;
                            apiException.printStackTrace();
                            int statusCode = apiException.getStatusCode();
                            Log.i("mytag", "place not found: " + e.getMessage());
                            Log.i("mytag", "status code: " + statusCode);
                        }
                    }
                });
            }

            @Override
            public void OnItemDeleteListener(int position, View v) {

            }
        });
        //TODO:fine prove

    }


    private void initComponentView() {
        this.dbManager= new UserDBManager(HamburgerActivity.this);
        this.userUsername=findViewById(R.id.utenteInformation);
        setMapComponent();
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.hamburger, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_modify) {
            Intent intentModify = new Intent(HamburgerActivity.this, ModificaProfiloActivity.class);
            startActivity(intentModify);
        } else if (id == R.id.nav_filtraRicerca) {
            CreateAlertDialagoWhitRadioButtonGroup();
        } else if (id == R.id.nav_logout) {
             Boolean check=this.dbManager.logout(this.userNamed);
             if(check) {
                 Intent intentLogout = new Intent(HamburgerActivity.this, MainActivity.class);
                 startActivity(intentLogout);
             }
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void CreateAlertDialagoWhitRadioButtonGroup(){

        AlertDialog.Builder builder = new AlertDialog.Builder(HamburgerActivity.this);
        builder.setTitle("Select Your Choice");


      builder.setSingleChoiceItems(values, -1, new DialogInterface.OnClickListener() {
          @Override
          public void onClick(DialogInterface dialog, int which) {
              selected=values[which];
          }
      });
        builder.setCancelable(false);

        builder.setPositiveButton("Save", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                    filter=selected;
                    Log.d("yle", ""+ filter);
                    filterSearch(filter);
                }
        });

        builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                mapFiltering=null;
            }
        });

        builder.setNeutralButton("Clear All", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                for(int i=0; i<checkedItems.length; i++){
                    checkedItems[i]=false;
                    selected=null;
                    mapFiltering=null;
                }
            }
        });

        alertDialog1 = builder.create();
        alertDialog1.show();
    }

    private void filterSearch(String filter) {
            switch(filter){

                case "Shopping":
                    String shopping="shopping_mall";
                    setFilter(shopping);
                    break;

                case "Restaurant":
                    String restaurant="restaurant";
                    setFilter(restaurant);
                    break;

                case "Bar":
                    String bar="bar";
                    setFilter(bar);
                    break;

                case "Museum":
                    String museum="museum";
                    setFilter(museum);
                    break;

                case "Library":
                    String library="library";
                    setFilter(library);
                    break;

                case "Train Station":
                    String train_station="train_station";
                    setFilter(train_station);
                    break;
            }
    }


    private void setFilter(String filtering) {
        Object dataTransfer[] = new Object[2];
        GetNearbyPlacesData getNearbyPlacesData = new GetNearbyPlacesData();
        //this.autocompleteSupportFragment.setText(filtering);

        String url = getUrl(place.latitude, place.longitude, filtering);
        dataTransfer[0] = mMap;
        dataTransfer[1] = url;

        getNearbyPlacesData.execute(dataTransfer);
        Toast.makeText(HamburgerActivity.this, "Showing Nearby " + filtering, Toast.LENGTH_SHORT).show();

    }



    private String getUrl(double latitude , double longitude , String nearbyPlace)
    {

        StringBuilder googlePlaceUrl = new StringBuilder("https://maps.googleapis.com/maps/api/place/nearbysearch/json?");
        googlePlaceUrl.append("location="+latitude+","+longitude);
        googlePlaceUrl.append("&radius="+PROXIMITY_RADIUS);
        googlePlaceUrl.append("&type="+nearbyPlace);
        googlePlaceUrl.append("&sensor=true");
        googlePlaceUrl.append("&key="+"AIzaSyBLEPBRfw7sMb73Mr88L91Jqh3tuE4mKsE");

        Log.d("MapsActivity", "url = "+googlePlaceUrl.toString());

        return googlePlaceUrl.toString();
    }



    public static void setUserInfo(String  tuple){
        userNamed=tuple;
        Log.d("yle", "usernmedHAM" +userNamed);
    }

    private void setMapComponent(){
        locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);

        mapFragment.getMapAsync(this);
        if(!Places.isInitialized()){
            Places.initialize(getApplicationContext(), getApplicationContext().getString(R.string.google_maps_api));
        }
        placeClient=Places.createClient(this);

        token = AutocompleteSessionToken.newInstance();
        placeFields= Arrays.asList(Place.Field.ID, Place.Field.LAT_LNG, Place.Field.NAME);

        //TODO:da togliere per autocomplete
        // autocompleteSupportFragment=(AutocompleteSupportFragment) getSupportFragmentManager().findFragmentById(R.id.autocomplete_fragment);
        //autocompleteSupportFragment.setPlaceFields(placeFields);
        //TODO:fine
        materialSearchBar = findViewById(R.id.searchBar);



        city=dbManager.getCityOfUser();
        Log.d("yle", "usercities"+city);
        Geocoder geo=new Geocoder(this);
    }


    private void addMarkerMy(double lat, double longi){
        LatLng city= new LatLng(lat, longi);
        place=city;
        marker=new MarkerOptions().position(city).title("Marker in UserCity");
        mMap.addMarker(marker);
        mMap.moveCamera(CameraUpdateFactory.newLatLng(city));
        Log.d("yle", "SONO IN ADD MARKER MY"+ marker.getPosition());
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        Geocoder geo = new Geocoder(this);
        UserDBManager dbManager = new UserDBManager(this);
        this.location = getLastLocation();

        mMap.setOnMyLocationButtonClickListener(new GoogleMap.OnMyLocationButtonClickListener() {
            @Override
            public boolean onMyLocationButtonClick() {
                if (materialSearchBar.isSuggestionsVisible())
                    materialSearchBar.clearSuggestions();
                if (materialSearchBar.isSearchEnabled())
                    materialSearchBar.disableSearch();
                return false;
            }
        });


        if (this.location != null) {
            double lat = location.getLatitude();
            double log = location.getLongitude();
            addMarkerMy(lat, log);

        } else {
            try {
                if(city!=null) {
                    lista = geo.getFromLocationName(city, 5);
                    Log.d("yle", "" + dbManager.getCityOfUser());
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            if(lista.get(0)!=null){
                double latitudine=lista.get(0).getLatitude();
                double longitudine= lista.get(0).getLongitude();
                addMarkerMy(latitudine,longitudine);
            }
        }
    }

    private Location getLastLocation() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED )
            locationManager.requestSingleUpdate(LocationManager.GPS_PROVIDER, new LocationListener() {
                @Override
                public void onLocationChanged(Location location) {

                }

                @Override
                public void onStatusChanged(String provider, int status, Bundle extras) {

                }

                @Override
                public void onProviderEnabled(String provider) {

                }

                @Override
                public void onProviderDisabled(String provider) {

                }
            }, null);
            return locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
    }


    public SupportMapFragment getSupportMap(){
        return this.mapFragment;
    }

    public PlacesClient getPlace(){
        return this.placeClient;
    }

    public List<Place.Field> getListaPlace(){
        return this.placeFields;
    }


    public AutocompleteSupportFragment getAutocompl(){
        return this.autocompleteSupportFragment;
    }

}
