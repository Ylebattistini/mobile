package com.example.citymap.main.local_db;

import android.content.ContentValues;
import android.database.Cursor;
import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Objects;

public class User implements Parcelable {

    private Integer id;
    private String name;
    private String surname;
    private String city;
    private String username;
    private String password;
    private static String finalUsername;

    public User() {
    }

    public User(String name, String surname, String city, String username, String password){
        this.id = null;
        this.name = name;
        this.surname = surname;
        this.city = city;
        this.username=username;
        this.password=password;

        finalUsername=this.username;
    }

    public static User from(JSONObject jsonObject){
        User utente = new User();
        try {
            utente.setId(jsonObject.getInt("id"));
            utente.setName(jsonObject.getString("name"));
            utente.setSurname(jsonObject.getString("surname"));
            utente.setCity(jsonObject.getString("city"));
            utente.setUsername(jsonObject.getString("username"));
            utente.setPassword(jsonObject.getString("password"));

        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
        return utente;
    }


    public static User from(Cursor cursor){
        User utente = new User();
        utente.setId(cursor.getInt(cursor.getColumnIndex(UserDescriptor.USER_ID)));
        utente.setName(cursor.getString(cursor.getColumnIndex(UserDescriptor.USER_NAME)));
        utente.setSurname(cursor.getString(cursor.getColumnIndex(UserDescriptor.USER_SURNAME)));
        utente.setCity(cursor.getString(cursor.getColumnIndex(UserDescriptor.USER_CITY)));
        utente.setUsername(cursor.getString(cursor.getColumnIndex(UserDescriptor.USER_USERNAME)));
        utente.setPassword(cursor.getString(cursor.getColumnIndex(UserDescriptor.USER_PASSWORD)));

        return utente;
    }

    public User(Parcel in) {
        this.id = in.readInt();
        this.name = in.readString();
        this.surname = in.readString();
        this.city = in.readString();
        this.surname=in.readString();
        this.password=in.readString();

    }

    public static final Creator<User> CREATOR = new Creator<User>() {
        @Override
        public User createFromParcel(Parcel in) {
            return new User(in);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String cities) {
        this.city = cities;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setUsername(String usernamee) {
        this.username = usernamee;
    }

    public String getUsername() {
        return username;
    }

    public void setPassword(String pass) {
        this.password = pass;
    }

    public String getPassword() {
        return password;
    }

    public static void setFinalUsername(String use) {
        finalUsername = use;
    }

    public String getFinalUsername() { return finalUsername; }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.name);
        dest.writeString(this.surname);
        dest.writeString(this.city);
        dest.writeString(this.username);
        dest.writeString(this.password);

    }


    public JSONObject toJson(){
        //N.B ATTENZIONE A DARE AI CAMPI LO STESSO NOME DEL jSON ottenuto mediante la risposta

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("id", getId());
            jsonObject.put("name", getName());
            jsonObject.put("surname", getSurname());
            jsonObject.put("city", getCity());
            jsonObject.put("username", getUsername());
            jsonObject.put("password", getPassword());

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }


    //------METODI AGGIUNTI PER LA GESTIONE DEL DB-----

    //metodo che a partire dall' oggetto mi costruisce un ContentValues
    // ContentValues è una mappa che contiene come keys le colonne della tabella e come values i valori che vogliamo avere nelle righe
    public ContentValues getContentValues(){
        ContentValues contentValues = new ContentValues();
        contentValues.put(UserDescriptor.USER_ID, this.id);
        contentValues.put(UserDescriptor.USER_NAME, this.name);
        contentValues.put(UserDescriptor.USER_SURNAME, this.surname);
        contentValues.put(UserDescriptor.USER_USERNAME, this.username);
        contentValues.put(UserDescriptor.USER_PASSWORD, this.password);

        return contentValues;
    }
}
