package com.example.citymap.main.local_db;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBHelper extends SQLiteOpenHelper {

    private static DBHelper DBHelperInstance = null;
    public static final String DB_NAME = "USER_DB";
    public static final int DB_VERSION = 1;

    public static synchronized DBHelper getInstance(Context context){
        if(DBHelperInstance==null){

            DBHelperInstance = new DBHelper(context.getApplicationContext());
        }
        return DBHelperInstance;
    }

    public DBHelper(Context context){
        super(context, DB_NAME, null, DB_VERSION);
    }

    static DBHelper inMemoryDb(Context context) {
        return new DBHelper(context, null, true);
    }
    static DBHelper fromName(Context context, String dbName) {
        return new DBHelper(context, dbName, false);
    }

    private DBHelper(Context context, String dbName, boolean inMem){
        super(context, inMem ? null : dbName, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(UserDescriptor.CREATE_TABLE_CHAT_URL);
        sqLiteDatabase.execSQL(UserDescriptor.CREATE_TABLE_SESSION);
    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + UserDescriptor.TABLE_USERS);
    }


    public Cursor getData() {
        SQLiteDatabase db=this.getWritableDatabase();
        String query= "SELECT * FROM "+ UserDescriptor.TABLE_USERS;
        Cursor data=db.rawQuery(query, null);
        return data;
    }

    public Cursor getSessionData() {
        SQLiteDatabase db=this.getWritableDatabase();
        String query= "SELECT * FROM "+ UserDescriptor.TABLE_SESSION;
        Cursor data=db.rawQuery(query, null);
        return data;
    }
}


