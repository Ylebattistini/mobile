package com.example.citymap.main;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.media.Image;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.citymap.R;
import com.example.citymap.main.Maps.HamburgerActivity;
import com.example.citymap.main.Maps.PermissionRequest;
import com.example.citymap.main.local_db.User;
import com.example.citymap.main.local_db.UserDBManager;
import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Arrays;

public class RegisterActivity extends AppCompatActivity {

    private CallbackManager callbackManager;
    private TextView nome;
    private TextView cognome;
    private TextView citta;
    private TextView username;
    private TextView password;
    private TextView confirmedPassword;
    private String nomeString;
    private String cognomeString;
    private String cittaString;
    private String usernameString;
    private String passwordString;
    private String confirmedPasswordString;
    private Button enter;
    private LoginButton registratiConFacebook;
    private User utente;
    private boolean insertData;
    private Intent intent;
    Uri imageUri;

    private ImageView immProfile;

    private UserDBManager dbmanager;
    private static final int REQUEST_CAMERA=1;
    private static final int SELECT_FILE=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registrazione);
        initComponentView();

        this.immProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedImage();
            }
        });

        this.registratiConFacebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               loginWithFacebook();
            }
        });


        this.enter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(nome.getText().toString().isEmpty() || cognome.getText().toString().isEmpty() || citta.getText().toString().isEmpty() || username.getText().toString().isEmpty() || password.getText().toString().isEmpty() || confirmedPassword.getText().toString().isEmpty())

                {
                    Toast.makeText(RegisterActivity.this, "Campi obbligatori non compilati", Toast.LENGTH_LONG).show();

                }else{
                    nomeString=nome.getText().toString();
                    cognomeString=cognome.getText().toString();
                    cittaString=citta.getText().toString();
                    usernameString=username.getText().toString();
                    passwordString=password.getText().toString();
                    confirmedPasswordString=confirmedPassword.getText().toString();
                    if(confirmedPasswordString.equals(passwordString)){
                        dbmanager= new UserDBManager(RegisterActivity.this);
                        if(dbmanager.checkUsername(usernameString)){
                            Toast.makeText(RegisterActivity.this, "Username già esistente!", Toast.LENGTH_LONG).show();
                        }
                        utente= new User(nomeString, cognomeString, cittaString, usernameString, passwordString);
                        registerUser(utente);
                    }else{
                        Toast.makeText(RegisterActivity.this, "Le password non coincidono", Toast.LENGTH_LONG).show();
                        confirmedPassword.setText("");
                        password.setText("");
                    }
                }
            }
        });
    }

    private void registerUser(User users) {
        Toast.makeText(this, "Ti registro nel db!", Toast.LENGTH_LONG).show();
        Log.d("yle", "registro i dati");
        Log.d("yle", ""+users);
        Log.d("yle", ""+users.getName());
        this.dbmanager= new UserDBManager(this);
        this.insertData= this.dbmanager.insert(users);
        if(insertData){
            Toast.makeText(this, "Ok ho registrato!", Toast.LENGTH_LONG).show();
            this.dbmanager.setSession(users);
            User.setFinalUsername(this.usernameString);
            HamburgerActivity.setUserInfo(this.usernameString);
            Toast.makeText(this, "Ok sessione", Toast.LENGTH_LONG).show();
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED ) {
                intent = new Intent(RegisterActivity.this, HamburgerActivity.class);
                startActivity(intent);
            }else{
                intent = new Intent(RegisterActivity.this, PermissionRequest.class);
                startActivity(intent);
            }
        }else{
            Toast.makeText(this, "Errore!!!", Toast.LENGTH_LONG).show();
        }
    }



    private void loginWithFacebook() {
        this.registratiConFacebook.setReadPermissions(Arrays.asList("user_hometown"));
        this.callbackManager = CallbackManager.Factory.create();

        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        if(Profile.getCurrentProfile() == null) {
                            new ProfileTracker() {
                                @Override
                                protected void onCurrentProfileChanged(Profile oldProfile, Profile currentProfile) {
                                    Profile.setCurrentProfile(currentProfile);
                                    setContents();
                                    this.stopTracking();
                                }
                            };
                        }
                        else{
                            setContents();
                        }
                    }

                    @Override
                    public void onCancel() {
                        Toast.makeText(getApplicationContext(),"Cancellato",Toast.LENGTH_LONG).show();
                    }
                    @Override
                    public void onError(FacebookException exception) {
                        Toast.makeText(getApplicationContext(),"Errore:"+exception.getMessage(),Toast.LENGTH_LONG).show();
                    }
                });
    }


    private void setContents(){
        Profile p = Profile.getCurrentProfile();
        getUserDetailsFromFB(AccessToken.getCurrentAccessToken());
        this.nome.setText(p.getFirstName());
        this.cognome.setText(p.getLastName());
        Glide.with(getApplicationContext())
                .load(p.getProfilePictureUri(dpToPx(150), dpToPx(150)))
                .fallback(R.drawable.ic_launcher_foreground)
                .into((ImageView)findViewById(R.id.imageView));

        new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(AccessToken oldAccessToken,
                                                       AccessToken currentAccessToken) {
                if (currentAccessToken == null) {
                    this.stopTracking();
                }
            }
        };
    }

    private void initComponentView() {
        this.nome=findViewById(R.id.nomeUtente);
        this.cognome=findViewById(R.id.cognomeUtente);
        this.citta=findViewById(R.id.cittaUtente);
        this.username=findViewById(R.id.userNameUtente);
        this.password=findViewById(R.id.passwordUtente);
        this.confirmedPassword=findViewById(R.id.confermaPassword);
        this.enter=findViewById(R.id.entra);
        this.registratiConFacebook=findViewById(R.id.registraticonFacebook);
        this.immProfile=findViewById(R.id.immUserReg);
        this.nomeString="";
        this.cognomeString="";
        this.cittaString="";
        this.usernameString="";
        this.passwordString="";
        this.confirmedPasswordString="";
    }

    public void getUserDetailsFromFB(AccessToken accessToken) {

        GraphRequest req=GraphRequest.newMeRequest(accessToken, new GraphRequest.GraphJSONObjectCallback() {
            @Override
            public void onCompleted(JSONObject object, GraphResponse response) {


                try{
                    String hometown = object.getJSONObject("hometown").getString("name");
                    citta.setText("Città:" + hometown);
                }catch (JSONException e)
                {
                    Toast.makeText(getApplicationContext(),"graph request error : "+e.getMessage(),Toast.LENGTH_SHORT).show();

                }

            }
        });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "hometown");
        req.setParameters(parameters);
        req.executeAsync();
    }


    private int dpToPx(int dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }


    private void selectedImage(){
        final CharSequence[] items={"Camera", "Gallery", "Cancel"};

        AlertDialog.Builder builder= new AlertDialog.Builder(RegisterActivity.this);
        builder.setTitle("Aggiungi immagine");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int i) {
                if(items[i].equals("Camera")){

                    Intent intent=new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(intent, REQUEST_CAMERA);

                }else if(items[i].equals("Gallery")){

                    Intent intent=new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    intent.setType("image/*");
                    startActivityForResult(intent.createChooser(intent, "Seleziona un'immagine"), SELECT_FILE);

                }else if (items[i].equals("Cancel")){
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode,resultCode,data);

        if(resultCode== Activity.RESULT_OK){

            if(requestCode==REQUEST_CAMERA){

                Bundle bundle= data.getExtras();
                final Bitmap bitmap=(Bitmap) bundle.get("data");
                Log.d("yle", "imm"+bitmap);
                this.immProfile.setImageBitmap(bitmap);

            }else {
                if (requestCode == SELECT_FILE) {
                    Uri selImage=data.getData();
                    this.immProfile.setImageURI(selImage);
                    Log.d("yle", "imm"+selImage);
                }
            }
        }
    }

}
