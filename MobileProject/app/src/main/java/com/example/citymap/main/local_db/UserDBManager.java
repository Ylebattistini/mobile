package com.example.citymap.main.local_db;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.widget.Toast;

import com.example.citymap.main.Maps.HamburgerActivity;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class UserDBManager implements DBManager<User, Integer>{

    private DBHelper dbHelper;
    private String finalUser;

    public UserDBManager(Context context){
        dbHelper = DBHelper.getInstance(context);
    }

    UserDBManager(DBHelper dbHelper) {
        this.dbHelper = dbHelper;
    }

    /**
     * costruisce una stringa che indica la clausola where a partire da un set di colonne su cui fare la selezione.
     * @param columnNames set con i nomi delle colonne su cui basare la costruzione della clausola where in formato stringa
     * @return la stringa corrispondente alla clausola where
     */
    private String buildWhereClause(Set<String> columnNames){
        StringBuilder result = new StringBuilder();
        boolean first = true;
        for(String columnName: columnNames) {
            if (first) {
                first = false;
            } else {
                result.append(" AND ");
            }
            result.append(columnName);
            result.append(" = ");
            result.append("?");
        }
        return result.toString();
    }

    private List<User> getListByCursor(Cursor cursor) {
        List<User> usersResultList = new ArrayList<>();
        if (cursor == null || !cursor.moveToFirst()) return usersResultList;
        do {
            usersResultList.add(User.from(cursor));
        } while (cursor.moveToNext());
        cursor.close();
        return usersResultList;
    }

    @Override
    public boolean save(User tuple) {
        if(findOne(tuple.getId())!=null){
            return update(tuple);
        }else{
            return insert(tuple);
        }
    }

    @Override
    public boolean save(List<User> tuples) {
        for(User entity:tuples){
            if(!save(entity)){
                return false;
            }
        }
        return true;
    }

    @Override
    public boolean insert(User tuple) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues contentvalues= new ContentValues();
        contentvalues.put(UserDescriptor.USER_NAME, tuple.getName());
        contentvalues.put(UserDescriptor.USER_SURNAME, tuple.getSurname());
        contentvalues.put(UserDescriptor.USER_CITY, tuple.getCity());
        contentvalues.put(UserDescriptor.USER_USERNAME, tuple.getUsername());
        contentvalues.put(UserDescriptor.USER_PASSWORD, tuple.getPassword());
        long result = db.insert(UserDescriptor.TABLE_USERS, null, contentvalues);
       if(result==-1){
           return false;
       }else{
           return true;
       }
    }

    public User getUserInformation(){
        User user = null;
        SQLiteDatabase db=dbHelper.getWritableDatabase();
        Cursor cursor1 = db.rawQuery("SELECT * FROM " + UserDescriptor.TABLE_SESSION + " WHERE " + UserDescriptor.SESSION_VERIFY + "=?", new String[]{"si"});
        if(cursor1.moveToFirst() && cursor1.getCount() > 0) {
            Cursor cursor2 = db.rawQuery("SELECT * FROM " + UserDescriptor.TABLE_USERS + " WHERE " + UserDescriptor.USER_USERNAME + "=?", new String[]{cursor1.getString(cursor1.getColumnIndex(UserDescriptor.SESSION_USERNAME))});
            user = (!cursor2.moveToFirst()) ? null : new User(cursor2.getString(cursor2.getColumnIndex(UserDescriptor.USER_NAME)), cursor2.getString(cursor2.getColumnIndex(UserDescriptor.USER_SURNAME)),
                    cursor2.getString(cursor2.getColumnIndex(UserDescriptor.USER_CITY)), cursor2.getString(cursor2.getColumnIndex(UserDescriptor.USER_USERNAME)), cursor2.getString(cursor2.getColumnIndex(UserDescriptor.USER_PASSWORD)));
            cursor2.close();
        }
        cursor1.close();
        return user;
    }

    @Override
    public boolean update(User tuple) {
        String stringa = null;
        int id=0;
        SQLiteDatabase db=dbHelper.getWritableDatabase();
        Cursor cursor1 = db.rawQuery("SELECT * FROM " + UserDescriptor.TABLE_SESSION + " WHERE " + UserDescriptor.SESSION_VERIFY + "=?", new String[]{"si"});
        if(cursor1.moveToFirst() && cursor1.getCount() > 0) {
            Cursor cursor2 = db.rawQuery("SELECT * FROM " + UserDescriptor.TABLE_USERS + " WHERE " + UserDescriptor.USER_USERNAME + "=?", new String[]{cursor1.getString(cursor1.getColumnIndex(UserDescriptor.SESSION_USERNAME))});
            stringa = (!cursor2.moveToFirst()) ? null : cursor2.getString(cursor2.getColumnIndex(UserDescriptor.USER_USERNAME));
            cursor2.close();

        }
            ContentValues contentvalues = new ContentValues();
            ContentValues contentvalues1 = new ContentValues();
            contentvalues1.put(UserDescriptor.SESSION_USERNAME, tuple.getName());
            contentvalues.put(UserDescriptor.USER_NAME, tuple.getName());
            contentvalues.put(UserDescriptor.USER_SURNAME, tuple.getSurname());
            contentvalues.put(UserDescriptor.USER_CITY, tuple.getCity());
            contentvalues.put(UserDescriptor.USER_USERNAME, tuple.getUsername());
            contentvalues.put(UserDescriptor.USER_PASSWORD, tuple.getPassword());
            db.update(UserDescriptor.TABLE_USERS, contentvalues, UserDescriptor.USER_ID+ "=?", new String[]{getUserId()});
            db.update(UserDescriptor.TABLE_SESSION, contentvalues1, UserDescriptor.SESSION_USERNAME + "=?" , new String[]{stringa});
            cursor1.close();
            HamburgerActivity.setUserInfo(tuple.getUsername());
            User.setFinalUsername(tuple.getUsername());
        return true;
    }

    private void updateSession(String finalUsername, String username) {
        SQLiteDatabase dbs=dbHelper.getWritableDatabase();
        Cursor cursor=dbs.rawQuery("SELECT * FROM " + UserDescriptor.TABLE_SESSION + " WHERE " + UserDescriptor.SESSION_USERNAME + "=?", new String[]{finalUsername});
        if(cursor.moveToFirst() && cursor.getCount() > 0) {
            ContentValues contentvalues = new ContentValues();
            contentvalues.put(UserDescriptor.SESSION_USERNAME, username);
            dbs.update(UserDescriptor.TABLE_SESSION, contentvalues, UserDescriptor.SESSION_USERNAME+ "=?",  new String[]{finalUsername});

        }
        cursor.close();
    }

    public boolean logout(String usernamed) {
        if (usernamed != null) {
            SQLiteDatabase dbs = dbHelper.getWritableDatabase();
            Cursor cursor = dbs.rawQuery("SELECT * FROM " + UserDescriptor.TABLE_SESSION + " WHERE " + UserDescriptor.SESSION_VERIFY + "=? AND " + UserDescriptor.SESSION_USERNAME + "=?", new String[]{"si", usernamed});
            if (cursor.moveToFirst() && cursor.getCount() > 0) {
                ContentValues contentvalues = new ContentValues();
                contentvalues.put(UserDescriptor.SESSION_VERIFY, "no");
                dbs.update(UserDescriptor.TABLE_SESSION, contentvalues, UserDescriptor.SESSION_USERNAME + "=?", new String[]{usernamed});
                return true;
            } else {
                return false;
            }
        } else {
            SQLiteDatabase dbs = dbHelper.getReadableDatabase();
            Cursor cursor = dbs.rawQuery("SELECT * FROM " + UserDescriptor.TABLE_SESSION + " WHERE " + UserDescriptor.SESSION_VERIFY + "=?", new String[]{"si"});
            if (cursor.getCount() > 0) {
                SQLiteDatabase db = dbHelper.getWritableDatabase();
                ContentValues contentvalues = new ContentValues();
                contentvalues.put(UserDescriptor.SESSION_VERIFY, "no");
                db.update(UserDescriptor.TABLE_SESSION, contentvalues, UserDescriptor.SESSION_USERNAME + "=?", new String[]{usernamed});
                return true;
            } else {
                return false;
            }
        }
    }

    public void setSession(User user) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM " + UserDescriptor.TABLE_SESSION + " WHERE " + UserDescriptor.SESSION_USERNAME + "=?", new String[]{user.getFinalUsername()});
        if(cursor.getCount()<=0 ) {
            ContentValues contentvalues = new ContentValues();
            contentvalues.put(UserDescriptor.SESSION_USERNAME, user.getFinalUsername());
            contentvalues.put(UserDescriptor.SESSION_VERIFY, "si");
            long result = db.insert(UserDescriptor.TABLE_SESSION, null, contentvalues);
        }else{
            ContentValues contentvalues = new ContentValues();
            contentvalues.put(UserDescriptor.SESSION_USERNAME, user.getFinalUsername());
            contentvalues.put(UserDescriptor.SESSION_VERIFY, "si");
            db.update(UserDescriptor.TABLE_SESSION, contentvalues, UserDescriptor.SESSION_USERNAME + "=?",  new String[]{user.getUsername()});
        }
    }

    public void setSessione(String username) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues contentvalues = new ContentValues();
        contentvalues.put(UserDescriptor.SESSION_VERIFY, "si");
        db.update(UserDescriptor.TABLE_SESSION, contentvalues, UserDescriptor.SESSION_USERNAME + "=?",  new String[]{username});
    }

    @Override
    public boolean update(String tableName, User tuple, String whereCondition, String[] whereArgument) {
        /* SQLiteDatabase implementa SQLClosable , mediante il quale posso utillare questo costrutto (zucchero sintattico disponibile da java 7) che automaticamente chiude
            il db all'uscita del blocco try, dopo aver fatto la return.
            Alternativa più elegante all'utilizzo di -> try { eseguo query } finally { db.close(); }
            */
            SQLiteDatabase db = dbHelper.getWritableDatabase();
            int result = db.update(tableName, tuple.getContentValues(), whereCondition, whereArgument);
            return result>0;

    }

    @Override
    public User findOne(Integer id) {
        if(id != null) {
            SQLiteDatabase db = dbHelper.getWritableDatabase();
        /* Cursor implementa Closable, mediante il quale posso utillare questo costrutto (zucchero sintattico disponibile da java 7) che automaticamente chiude
            il cursor all'uscita del blocco try, dopo aver fatto la return.
            Alternativa più elegante all'utilizzo di -> try { .. } finally { cursor.close(); }
        */
            try (Cursor c = db.rawQuery(getSelectByIdQueryString(), new String[]{id.toString()})) {
                return (!c.moveToFirst()) ? null : User.from(c);
            }
        }
        return null;
    }

    @Override
    public boolean delete(String whereCondition, String[] whereArgument) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        int count = db.delete(UserDescriptor.TABLE_USERS, whereCondition, whereArgument);
        return count >= 1;
    }

    @Override
    public boolean delete(Integer id) {
        return delete(UserDescriptor.USER_ID+" = ?", new String[]{id.toString()});
    }

    @Override
    public boolean delete(User tuple) {
        return delete(UserDescriptor.USER_ID+" = ?", new String[]{getUserId()});
    }

    private String getUserId() {
        int id = 0;
        String idies=null;
        SQLiteDatabase db=dbHelper.getWritableDatabase();
        Cursor cursor1 = db.rawQuery("SELECT * FROM " + UserDescriptor.TABLE_SESSION + " WHERE " + UserDescriptor.SESSION_VERIFY + "=?", new String[]{"si"});
        if(cursor1.moveToFirst() && cursor1.getCount() > 0) {
            Cursor cursor2 = db.rawQuery("SELECT * FROM " + UserDescriptor.TABLE_USERS + " WHERE " + UserDescriptor.USER_USERNAME + "=?", new String[]{cursor1.getString(cursor1.getColumnIndex(UserDescriptor.SESSION_USERNAME))});
            id = (!cursor2.moveToFirst()) ? null : (cursor2.getInt(cursor2.getColumnIndex(UserDescriptor.USER_ID)));
            cursor2.close();
        }cursor1.close();
        idies= String.valueOf(id);
        return idies;
    }

    @Override
    public List<User> findAll(Map<String, String> whereCondition) {
        SQLiteDatabase sqLiteDatabase = dbHelper.getReadableDatabase();
        String[] whereConditionArray = whereCondition.values().toArray(new String[0]);
        Cursor c = sqLiteDatabase.query(UserDescriptor.TABLE_USERS, null, buildWhereClause(whereCondition.keySet()), whereConditionArray, null, null, null);
        List<User> userListResult = getListByCursor(c);
        sqLiteDatabase.close();
        return userListResult;
    }

    @Override
    public List<User> findAll() {
        SQLiteDatabase sqLiteDatabase = dbHelper.getReadableDatabase();
        Cursor c = sqLiteDatabase.rawQuery("SELECT * FROM " + UserDescriptor.TABLE_USERS, null);
        List<User> userListResult = getListByCursor(c);
        sqLiteDatabase.close();
        return userListResult;
    }

    @Override
    public List<User> findAll(String whereCondition, String[] values, String orderBy) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        Cursor c = db.query(UserDescriptor.TABLE_USERS, null, whereCondition, values, null , null , orderBy);
        List<User> userListResult = getListByCursor(c);
        return userListResult;
    }

    private String getSelectByIdQueryString(){
        return "SELECT * FROM "+ UserDescriptor.TABLE_USERS+ " WHERE "+ UserDescriptor.USER_ID + "= ?";
    }


    public boolean checkUsername(String username){
        SQLiteDatabase db=dbHelper.getReadableDatabase();
        Cursor cursor=db.rawQuery("SELECT * FROM " + UserDescriptor.TABLE_USERS + " WHERE " + UserDescriptor.USER_USERNAME + "=?", new String[]{username});
        if(cursor.getCount()>0) return true;
        else return false;
    }


    public boolean checkPassword(String password){
        SQLiteDatabase db=dbHelper.getReadableDatabase();
        Cursor cursor=db.rawQuery("SELECT * FROM " + UserDescriptor.TABLE_USERS + " WHERE " + UserDescriptor.USER_PASSWORD + "=?", new String[]{password});
        if(cursor.getCount()>0) return true;
        else return false;
    }


    public boolean isLogged() {
        SQLiteDatabase db=dbHelper.getReadableDatabase();
        Cursor cursor=db.rawQuery("SELECT * FROM " + UserDescriptor.TABLE_SESSION + " WHERE " + UserDescriptor.SESSION_VERIFY + "=?", new String[]{"si"});
        if(cursor.getCount()>0) return true;
        else return false;
    }

    public String getUserUsernameLogged() {
        String stringa = null;
        SQLiteDatabase db=dbHelper.getWritableDatabase();
        Cursor cursor1 = db.rawQuery("SELECT * FROM " + UserDescriptor.TABLE_SESSION + " WHERE " + UserDescriptor.SESSION_VERIFY + "=?", new String[]{"si"});
        if(cursor1.moveToFirst() && cursor1.getCount() > 0) {
            Cursor cursor2 = db.rawQuery("SELECT * FROM " + UserDescriptor.TABLE_USERS + " WHERE " + UserDescriptor.USER_USERNAME + "=?", new String[]{cursor1.getString(cursor1.getColumnIndex(UserDescriptor.SESSION_USERNAME))});
            stringa = (!cursor2.moveToFirst()) ? null : cursor2.getString(cursor2.getColumnIndex(UserDescriptor.USER_USERNAME));
            cursor2.close();
        }
        return stringa;
    }


    public String getCityOfUser() {
        String city = null;
        SQLiteDatabase db=dbHelper.getWritableDatabase();
        Cursor cursor1 = db.rawQuery("SELECT * FROM " + UserDescriptor.TABLE_SESSION + " WHERE " + UserDescriptor.SESSION_VERIFY + "=?", new String[]{"si"});
        if(cursor1.moveToFirst() && cursor1.getCount() > 0) {
            Cursor cursor2 = db.rawQuery("SELECT * FROM " + UserDescriptor.TABLE_USERS + " WHERE " + UserDescriptor.USER_USERNAME + "=?", new String[]{cursor1.getString(cursor1.getColumnIndex(UserDescriptor.SESSION_USERNAME))});
            city = (!cursor2.moveToFirst()) ? null : cursor2.getString(cursor2.getColumnIndex(UserDescriptor.USER_CITY));
            cursor2.close();
        }
        return city;
    }

    public void setUsername(String name) {
        finalUser=name;
    }

}
