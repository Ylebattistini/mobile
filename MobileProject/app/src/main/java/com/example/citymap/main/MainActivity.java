package com.example.citymap.main;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.citymap.R;
import com.example.citymap.main.Maps.HamburgerActivity;
import com.example.citymap.main.local_db.UserDBManager;

public class MainActivity extends AppCompatActivity {

    private Button login;
    private Button registrazione;
    private Intent intent;

    private UserDBManager dbmanager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.dbmanager= new UserDBManager(MainActivity.this);
        if(!this.dbmanager.isLogged()) {
            setContentView(R.layout.activity_main);
            initComponentView();

            this.login.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    intent= new Intent(MainActivity.this, LoginActivity.class);
                    startActivity(intent);
                }
            });

            this.registrazione.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    intent= new Intent(MainActivity.this, RegisterActivity.class);
                    startActivity(intent);
                }
            });
        }else{
            intent= new Intent(MainActivity.this, HamburgerActivity.class);
            startActivity(intent);
        }

    }

    private void initComponentView() {
        this.login=findViewById(R.id.Login);
        this.registrazione=findViewById(R.id.Registrazione);
    }
}

