package com.example.citymap.main.Maps;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.util.Log;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public final class GeocodingUtils {

    /**
     * Get list of address by latitude and longitude
     *
     * @return null or List<Address>
     */
    public static List<Address> getGeocoderAddresses(Context context, Location location) {
        if (location != null) {
            Geocoder geocoder = new Geocoder(context, Locale.ITALY);
            try {
                List<Address> addresses = geocoder.getFromLocation(location.getLatitude(),
                        location.getLongitude(), 1);
                return addresses;
            } catch (IOException e) {
                // e.printStackTrace();
                Log.e("Error : Geocoder", "Impossible to connect to Geocoder",
                        e);
            }
        }

        return new ArrayList<>();
    }

    public static Address getGeocoderAddress(Context context, Location location) {
        final List<Address> addresses = getGeocoderAddresses(context, location);
        if(!addresses.isEmpty())
            return addresses.get(0);
        else
            return null;
    }

    /**
     * Try to get AddressLine
     *
     * @return null or addressLine
     */
    public static String getAddressLine(Context context, Location location) {
        Address address = getGeocoderAddress(context, location);
        if (address != null) {
            String addressLine = address.getAddressLine(0);

            return addressLine;
        } else {
            return null;
        }
    }

    /**
     * Try to get Locality
     *
     * @return null or locality
     */
    public static String getLocality(Context context, Location location) {
        Address address = getGeocoderAddress(context, location);
        if (address != null) {
            String locality = address.getLocality();

            return locality;
        } else {
            return null;
        }
    }

    /**
     * Try to get Postal Code
     *
     * @return null or postalCode
     */
    public static String getPostalCode(Context context, Location location) {
        Address address = getGeocoderAddress(context, location);
        if (address != null) {
            String postalCode = address.getPostalCode();

            return postalCode;
        } else {
            return null;
        }
    }

    /**
     * Try to get CountryName
     *
     * @return null or postalCode
     */
    public static String getCountryName(Context context, Location location) {
        Address address = getGeocoderAddress(context, location);
        if (address != null) {
            String countryName = address.getCountryName();

            return countryName;
        } else {
            return null;
        }
    }

}
