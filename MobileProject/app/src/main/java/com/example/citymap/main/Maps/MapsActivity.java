package com.example.citymap.main.Maps;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;

import com.example.citymap.R;
import com.example.citymap.main.local_db.DBManager;
import com.example.citymap.main.local_db.UserDBManager;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderClient;

import com.google.android.gms.location.LocationServices;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.AutocompletePrediction;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.libraries.places.api.model.AutocompleteSessionToken;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.model.TypeFilter;
import com.google.android.libraries.places.api.net.FindAutocompletePredictionsRequest;
import com.google.android.libraries.places.api.net.FindAutocompletePredictionsResponse;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.android.libraries.places.widget.AutocompleteSupportFragment;
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener;

import java.util.Arrays;
import java.util.List;

import android.annotation.SuppressLint;
import android.content.IntentSender;
import android.location.Location;
import android.os.Handler;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.libraries.places.api.net.FetchPlaceRequest;
import com.google.android.libraries.places.api.net.FetchPlaceResponse;


import java.util.ArrayList;

import android.Manifest;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.android.libraries.places.widget.AutocompleteSupportFragment;
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

/*
public class MapsActivity extends AppCompatActivity implements OnMapReadyCallback{



    private PlacesClient placeClient;
    private GoogleMap mMap;
    private LocationManager locationManager;

    List<Address> lista;
    private MarkerOptions marker;
    private Location location;

    private  SupportMapFragment mapFragment;
    private List<Place.Field> placeFields;
    private AutocompleteSupportFragment autocompleteSupportFragment;

    private String citta;

    private String filter;
    int PROXIMITY_RADIUS = 10000;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_hamburger);
        //Object dataTransfer[] = new Object[2];
        //GetNearbyPlacesData getNearbyPlacesData = new GetNearbyPlacesData();
        HamburgerActivity ham=new HamburgerActivity();
        mapFragment=ham.getSupportMap();
        placeClient=ham.getPlace();
        placeFields=ham.getListaPlace();
        autocompleteSupportFragment=ham.getAutocompl();


        Geocoder geo=new Geocoder(this);
        UserDBManager dbManager= new UserDBManager(this);

        mapFragment.getMapAsync(this);


        //filter=ham.getMapFiltering();
        //mapFragment.getMapAsync(this);

        //TODO:for filter non so se va
       /* if(filter!=null){
            mMap.clear();
            String url = getUrl(44.1391000, 12.2431500, filter);
            dataTransfer[0] = mMap;
            dataTransfer[1] = url;

            getNearbyPlacesData.execute(dataTransfer);
        }*/


        /*mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);


        if(!Places.isInitialized()){
            Places.initialize(getApplicationContext(), getApplicationContext().getString(R.string.google_maps_api));
        }
        placeClient=Places.createClient(this);

        // AutocompleteSessionToken token = AutocompleteSessionToken.newInstance();
        placeFields= Arrays.asList(Place.Field.ID, Place.Field.LAT_LNG, Place.Field.NAME);

        autocompleteSupportFragment=(AutocompleteSupportFragment) getSupportFragmentManager().findFragmentById(R.id.autocomplete_fragment);
        autocompleteSupportFragment.setPlaceFields(placeFields);

*//*
        autocompleteSupportFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(@NonNull Place place) {
                final LatLng latlang= place.getLatLng();
                Log.d("yle", "placeOK"+latlang.latitude +"\n"+latlang.longitude);
                addMarkerMy(latlang.latitude, latlang.longitude);
            }

            @Override
            public void onError(@NonNull Status status) {
                Log.d("yle", "noPlacepreso");
            }
        });

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        Geocoder geo=new Geocoder(this);
        UserDBManager dbManager= new UserDBManager(this);
        this.location=getLastLocation();
       if(this.location!=null){
            double lat= location.getLatitude();
            double log=location.getLongitude();
            addMarkerMy(lat, log);

        }else {
            try {
                lista = geo.getFromLocationName(dbManager.getCityOfUser(), 5);
                Log.d("yle", ""+ dbManager.getCityOfUser());
            } catch (IOException e) {
                e.printStackTrace();
            }

            Log.d("yleCity", ""+dbManager.getCityOfUser());
            double latitudine=lista.get(1).getLatitude();
            double longitudine= lista.get(1).getLongitude();
            addMarkerMy(latitudine, longitudine);
        }

    }



    private Location getLastLocation() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED )
            return locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        return null;
    }


    private void addMarkerMy(double lat, double longi){
        LatLng city= new LatLng(lat, longi);
        marker=new MarkerOptions().position(city).title("Marker in UserCity");
        mMap.addMarker(marker);
        mMap.moveCamera(CameraUpdateFactory.newLatLng(city));
    }

    private String getUrl(double latitude , double longitude , String nearbyPlace)
    {

        StringBuilder googlePlaceUrl = new StringBuilder("https://maps.googleapis.com/maps/api/place/nearbysearch/json?");
        googlePlaceUrl.append("location="+latitude+","+longitude);
        googlePlaceUrl.append("&radius="+PROXIMITY_RADIUS);
        googlePlaceUrl.append("&type="+nearbyPlace);
        googlePlaceUrl.append("&sensor=true");
        googlePlaceUrl.append("&key="+ getApplicationContext().getString(R.string.google_maps_api));

        Log.d("MapsActivity", "url = "+null);//googlePlaceUrl.toString());

        return googlePlaceUrl.toString();
    }

}*/
