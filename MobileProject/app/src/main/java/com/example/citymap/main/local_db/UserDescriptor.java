package com.example.citymap.main.local_db;

public final class UserDescriptor {

    /*classe con metodi e campi statici per accedere in maniera rapida alle proprietà di una tabella
        oltre che alla stringa utilizzata per la sua creazione
     */
    private UserDescriptor(){}

    public static final String TEXT_TYPE = " TEXT";

    //N.B -> IL SQLITE è POSSIBILE salvare solo dati di tipo string, float, double

    public static final String TABLE_USERS = "USER";
    public static final String TABLE_SESSION = "SESSION";

    public static final String USER_ID = "ID_USER";
    public static final String USER_NAME = "NAME";
    public static final String USER_SURNAME = "SURNAME";
    public static final String USER_CITY = "CITY";
    public static final String USER_USERNAME = "USERNAME";
    public static final String USER_PASSWORD = "PASSWORD";


    public static final String SESSION_ID = "ID_SESSION";
    public static final String SESSION_USERNAME = "SESSION_USERNAME";
    public static final String SESSION_VERIFY = "VERIFY";

    public static  final String CREATE_TABLE_CHAT_URL = "CREATE TABLE IF NOT EXISTS " + TABLE_USERS + " ( "+
            USER_ID + " INTEGER PRIMARY KEY ," +
            USER_NAME + TEXT_TYPE + " ," +
            USER_SURNAME + TEXT_TYPE + " ," +
            USER_CITY + TEXT_TYPE + " ," +
            USER_USERNAME + TEXT_TYPE + " ," +
            USER_PASSWORD + TEXT_TYPE + " )";



    public static  final String CREATE_TABLE_SESSION = "CREATE TABLE IF NOT EXISTS " + TABLE_SESSION + " ( "+
            SESSION_ID+ " INTEGER PRIMARY KEY ," +
            SESSION_USERNAME + TEXT_TYPE + " ," +
            SESSION_VERIFY + TEXT_TYPE + " )";

}

