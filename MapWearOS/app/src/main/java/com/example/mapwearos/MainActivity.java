package com.example.mapwearos;

import android.content.Intent;
import android.os.Bundle;
import android.support.wearable.activity.WearableActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.w3c.dom.Text;

public class MainActivity extends WearableActivity {

    private TextView title;
    private TextView subTitle;
    private Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        title = (TextView) findViewById(R.id.title);
        subTitle = (TextView) findViewById(R.id.subtitle);
        button = (Button) findViewById(R.id.entra);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("mapsActivity", "entrato");
                Intent intent = new Intent(MainActivity.this, Map.class);
                startActivity(intent);
            }
        });

        // Enables Always-on
        setAmbientEnabled();
    }
}
