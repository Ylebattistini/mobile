package com.example.mapwearos;

import android.os.Bundle;
import android.support.wear.widget.SwipeDismissFrameLayout;
import android.support.wearable.activity.WearableActivity;
import android.support.wearable.view.DismissOverlayView;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class Map extends WearableActivity
        implements OnMapReadyCallback, GoogleMap.OnMapLongClickListener {

    private MapFragment mMapFragment;
    private SwipeDismissFrameLayout mDismissOverlay;

    private static final LatLng SYDNEY = new LatLng(-33.85704, 151.21522);
    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);

        mDismissOverlay =
                (SwipeDismissFrameLayout) findViewById(R.id.swipe_dismiss_root_container);

        mMapFragment = (MapFragment) getFragmentManager()
                .findFragmentById(R.id.map);
        mMapFragment.getMapAsync(this);

        setAmbientEnabled();
    }

    @Override
    public void onEnterAmbient(Bundle ambientDetails) {
        super.onEnterAmbient(ambientDetails);
        mMapFragment.onEnterAmbient(ambientDetails);
    }


    public void onMapLongClick(LatLng point) {
       //mDismissOverlay.show();
    }


    @Override
    public void onMapReady(GoogleMap map) {
        mMap = map;
        mMap.addMarker(new MarkerOptions().position(SYDNEY)
                .title("Sydney Opera House"));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(SYDNEY, 10));
        mMap.setOnMapLongClickListener(this);
    }

    @Override
    public void onExitAmbient() {
        super.onExitAmbient();
        mMapFragment.onExitAmbient();
    }
}
